#!/usr/bin/env python

import os
import stat
import time
from munin import MuninPlugin

class GasPlugin(MuninPlugin):
    title = "Gas"
    args = "--base 1000 -l0"
    vlabel = "gas"
    scale = False
    category = "system"

    @property
    def fields(self):
        warning = 20
        critical = 50
        return [("gas", dict(
                label = "gas",
                info = 'The gas concentration as reported by the Hanwei MQ-2 gas sensor.',
                type = "GAUGE",
                min = "0",
                warning = str(warning),
                critical = str(critical)))]

    def execute(self):
        sensorFile = "/var/sensors/gas"
        if os.path.exists(sensorFile) and  time.time() - os.stat(sensorFile)[stat.ST_MTIME] <= 300:
            gas = open(sensorFile, "r").read()
            return dict(gas=gas)
        else:
            return dict(gas='U')


if __name__ == "__main__":
    GasPlugin().run()

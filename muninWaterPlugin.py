#!/usr/bin/env python

import os
import stat
import time
from munin import MuninPlugin

class WaterPlugin(MuninPlugin):
    title = "Water"
    args = "--base 1000 -l0"
    vlabel = "water"
    scale = False
    category = "system"

    @property
    def fields(self):
        warning = 1
        critical = 1
        return [("water", dict(
                label = "water",
                info = 'Water detected by the Grove SEN113104 water sensor.',
                type = "GAUGE",
                min = "0",
                warning = str(warning),
                critical = str(critical)))]

    def execute(self):
        sensorFile = "/var/sensors/water"
        if os.path.exists(sensorFile) and  time.time() - os.stat(sensorFile)[stat.ST_MTIME] <= 300:
            water = open(sensorFile, "r").read()
            return dict(water=water)
        else:
            return dict(water='U')


if __name__ == "__main__":
    WaterPlugin().run()

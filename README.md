# README #

This is a set of munin plugins designed to integrate output from the [sampleSensors.py](https://bitbucket.org/nsssystems/grove-integration-for-pi-monitor) program with [munin](http://munin-monitoring.org/).

### Set Up ###

Follow the directions to install the [sampleSensors.py](https://bitbucket.org/nsssystems/grove-integration-for-pi-monitor) program. 

Install munin packages:

    sudo apt-get install munin munin-node munin-async

Install python-munin module

    git clone https://github.com/samuel/python-munin.git
    cd python-munin
    sudo python setup.py

Further munin integration is left as an exercise for the reader.

### Contact Information ###

* This is a sample program developed by the UMass Enterprise Applications team.
* for more information, contact [Mark Scarbrough](marks@umass.edu)
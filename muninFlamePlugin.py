#!/usr/bin/env python

import os
import stat
import time
from munin import MuninPlugin

class FlamePlugin(MuninPlugin):
    title = "Flame"
    args = "--base 1000 -l0"
    vlabel = "flame"
    scale = False
    category = "system"

    @property
    def fields(self):
        warning = 1
        critical = 1
        return [("flame", dict(
                label = "flame",
                info = 'Fire detected by the YG1006 flame sensor.',
                type = "GAUGE",
                min = "0",
                warning = str(warning),
                critical = str(critical)))]

    def execute(self):
        sensorFile = "/var/sensors/flame"
        if os.path.exists(sensorFile) and  time.time() - os.stat(sensorFile)[stat.ST_MTIME] <= 300:
            flame = open(sensorFile, "r").read()
            return dict(flame=flame)
        else:
            return dict(flame='U')


if __name__ == "__main__":
    FlamePlugin().run()

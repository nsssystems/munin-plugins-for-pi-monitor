#!/usr/bin/env python

import os
import time
import stat
from munin import MuninPlugin

class TemperaturePlugin(MuninPlugin):
    title = "Temperature"
    args = "--base 1000 -l0"
    vlabel = "temperature"
    scale = False
    category = "system"

    @property
    def fields(self):
        warning = 25
        critical = 30
        return [("temperature", dict(
                label = "temperature",
                info = 'The temperature as reported by the AM-2302 temperature/humidity sensor.',
                type = "GAUGE",
                min = "0",
                warning = str(warning),
                critical = str(critical)))]

    def execute(self):
        sensorFile = "/var/sensors/temperature"
        if os.path.exists(sensorFile) and  time.time() - os.stat(sensorFile)[stat.ST_MTIME] <= 300:
            temperature = open(sensorFile, "r").read()
            return dict(temperature=temperature)
        else:
            return dict(temperature='U')

if __name__ == "__main__":
    TemperaturePlugin().run()

#!/usr/bin/env python

import os
import time
import stat
from munin import MuninPlugin

class DustPlugin(MuninPlugin):
    title = "Dust"
    args = "--base 1000 -l0"
    vlabel = "dust"
    scale = False
    category = "system"

    @property
    def fields(self):
        warning = 2000
        critical = 3000
        return [("dust", dict(
                label = "dust",
                info = 'The dust particle count as reported by the Shinyei Model PPD42NS Particle Sensor.',
                type = "GAUGE",
                min = "0",
                warning = str(warning),
                critical = str(critical)))]

    def execute(self):
        sensorFile = "/var/sensors/dust"
        if os.path.exists(sensorFile) and  time.time() - os.stat(sensorFile)[stat.ST_MTIME] <= 300:
            dust = open(sensorFile, "r").read()
            return dict(dust=dust)
        else:
            return dict(dust='U')

if __name__ == "__main__":
    DustPlugin().run()

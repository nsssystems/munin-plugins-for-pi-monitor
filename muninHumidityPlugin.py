#!/usr/bin/env python

import os
import time
import stat
from munin import MuninPlugin

class HumidityPlugin(MuninPlugin):
    title = "Humidity"
    args = "--base 1000 -l0"
    vlabel = "humidity"
    scale = False
    category = "system"

    @property
    def fields(self):
        warning = 80
        critical = 90
        return [("humidity", dict(
                label = "humidity",
                info = 'The humidity as reported by the AM-2302 temperature/humidity sensor.',
                type = "GAUGE",
                min = "0",
                warning = str(warning),
                critical = str(critical)))]

    def execute(self):
        sensorFile = "/var/sensors/humidity"
        if os.path.exists(sensorFile) and  time.time() - os.stat(sensorFile)[stat.ST_MTIME] <= 300:
            humidity = open(sensorFile, "r").read()
            return dict(humidity=humidity)
        else:
            return dict(humidity='U')


if __name__ == "__main__":
    HumidityPlugin().run()
